using UnityEngine;

public class Racer : MonoBehaviour
{
    private enum PlayerState
    {
        Waiting,
        Racing,
        Dodging,
        Dead
    };

    public delegate void RacerDied();
    public static RacerDied OnRacerDied;

    [SerializeField] private float MOVE_SPEED = 0.50f;
    [SerializeField] private float JUMP_FORCE = 95f;

    [SerializeField] private BoxCollider2D boxCollider;

    [SerializeField] private bool isPlayer = false;

    [SerializeField] private AudioClip jumpSFX;

    private bool isGrounded = false;
    private float aiIsGroundedCheckWaitTime = 0.25f;
    private float aiIsGroundedCheckWaitTimer = 0f;
    private float aiIsDodgingWaitTime = 0.3f;
    private float aiIsDodgingWaitTimer = 0f;

    private Run run;
    private Jump jump;
    private SpriteController spriteController;

    private PlayerState state = PlayerState.Waiting;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        run = GetComponent<Run>();
        jump = GetComponent<Jump>();
        spriteController = GetComponent<SpriteController>();
    }

    private void OnEnable()
    {
        FinishLine.OnFinishLineCrossed += OnFinishLineCrossed;
    }

    private void OnDisable()
    {
        FinishLine.OnFinishLineCrossed -= OnFinishLineCrossed;
    }

    private void Update()
    {
        CheckGrounded();

        if (state != PlayerState.Racing)
        {
            return;
        }

        PlayerUpdate();
        AIUpdate();
    }

    private void PlayerUpdate()
    {
        if (!isPlayer)
        {
            return;
        }

        Move();
        Jump();
    }

    private void AIUpdate()
    {
        if (isPlayer || IsDodging())
        {
            return;
        }

        if (DetectActiveSpikeTrap())
        {
            HandleActiveSpikeTrap();
        }
        else if (DetectInactiveSpikeTrap())
        {
            HandleInactiveSpikeTrap();
        }
        else if (DetectLedge())
        {
            Jump();
        }
        else
        {
            Move(1);
        }
    }

    private void Move(int direction = 0)
    {
        var velocity = SetVelocity(direction);
    }

    private void UpdateSpriteForMove(int direction)
    {
        spriteController.Flip(direction);
        spriteController.SetBool("isRunning", direction != 0 ? true : false);
    }

    private float SetVelocity(int direction)
    {
        var velocity = GetVelocity(direction);

        run.SetVelocity(velocity);
        return velocity;
    }

    private float GetVelocity(int direction)
    {
        int moveInput = 0;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveInput -= 1;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveInput += 1;
        }

        if (!isPlayer)
        {
            moveInput = direction;
        }

        UpdateSpriteForMove(moveInput);
        return MOVE_SPEED * moveInput;
    }

    private void CheckGrounded()
    {
        if (!isPlayer && aiIsGroundedCheckWaitTimer > 0f)
        {
            aiIsGroundedCheckWaitTimer -= Time.deltaTime;
            return;
        }

        isGrounded = jump.IsGrounded();
        spriteController.SetBool("isGrounded", isGrounded);
    }

    private void Jump()
    {
        if (isPlayer && !Input.GetKeyDown(KeyCode.Space))
        {
            return;
        }

        if (!isGrounded)
        {
            return;
        }

        jump.AddForce(JUMP_FORCE);
        isGrounded = false;
        aiIsGroundedCheckWaitTimer = aiIsGroundedCheckWaitTime;
        AudioSource.PlayClipAtPoint(jumpSFX, transform.position, GameSettings.Instance.GetVolume());
    }

    public void Kill(bool createGhost = true)
    {
        if (state == PlayerState.Dead)
        {
            return;
        }

        if (isPlayer && createGhost)
        {
            GameObject ghost = Resources.Load<GameObject>("Ghost");
            GameObject ghostInstance = Instantiate(ghost, transform.position, Quaternion.identity);
            ghostInstance.GetComponent<Ghost>().SetColor(spriteController.GetMaterial());
            CameraController cam = FindObjectOfType<CameraController>();
            cam.SetTarget(ghostInstance.transform);
        }

        spriteController.SetTrigger("dead");
        state = PlayerState.Dead;
        run.Freeze();

        OnRacerDied?.Invoke();
    }

    private void OnFinishLineCrossed(Racer racer)
    {
        if (racer != this)
        {
            Kill(false);
            return;
        }

        run.Freeze();
        state = PlayerState.Waiting;
        spriteController.SetTrigger("victory");
    }

    public void SetPlayer()
    {
        isPlayer = true;
        spriteController.SetSortOrder(10);
    }

    public bool IsPlayer()
    {
        return isPlayer;
    }

    public bool IsDead()
    {
        return state == PlayerState.Dead;
    }

    public Material GetColor()
    {
        return spriteController.GetMaterial();
    }

    public void SetColor(Material material)
    {
        spriteController.SetMaterial(material);
    }

    public void SetStateRacing()
    {
        state = PlayerState.Racing;
    }

    private bool DetectLedge()
    {
        float xPos = boxCollider.transform.position.x + boxCollider.bounds.extents.x + boxCollider.offset.x - 0.05f;
        float yPos = boxCollider.transform.position.y - boxCollider.bounds.extents.y + boxCollider.offset.y;
        Vector2 origin = new Vector2(xPos, yPos);

        LayerMask groundMask = LayerMask.GetMask("Ground");

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.down, 0.10f, groundMask);
        if (!hit)
        {
            return true;
        }
        
        return false;
    }

    private bool DetectActiveSpikeTrap()
    {
        Vector2 origin = GetActiveSpikeTrapDetectionOrigin();
        LayerMask activeTrap = LayerMask.GetMask("ActionableTrap");
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.down, 0.05f, activeTrap);
        if (hit.collider != null)
        {
            return hit.collider.gameObject.GetComponent<SpikeTrap>().TrapState == TrapState.Active;
        }
        return false;
    }

    private bool DetectInactiveSpikeTrap()
    {
        Vector2 origin = GetInactiveSpikeTrapDetectionOrigin();
        LayerMask inactiveTrap = LayerMask.GetMask("ActionableTrap");
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.down, 0.05f, inactiveTrap);
        if (hit.collider != null)
        {
            return hit.collider.gameObject.GetComponent<SpikeTrap>().TrapState == TrapState.Inactive;
        }
        return false;
    }

    private Vector2 GetLedgeDetectionOrigin()
    {
        float xPos = boxCollider.transform.position.x + boxCollider.bounds.extents.x + boxCollider.offset.x - 0.05f;
        float yPos = boxCollider.transform.position.y - boxCollider.bounds.extents.y + boxCollider.offset.y;
        return new Vector2(xPos, yPos);
    }

    private Vector2 GetSpikeTrapDetectionOrigin()
    {
        float xPos = boxCollider.transform.position.x + boxCollider.bounds.extents.x + boxCollider.offset.x;
        float yPos = boxCollider.transform.position.y - boxCollider.bounds.extents.y + boxCollider.offset.y;
        return new Vector2(xPos, yPos);
    }

    private Vector2 GetInactiveSpikeTrapDetectionOrigin()
    {
        float xPos = boxCollider.transform.position.x + boxCollider.bounds.extents.x + boxCollider.offset.x;
        float yPos = boxCollider.transform.position.y - boxCollider.bounds.extents.y + boxCollider.offset.y;
        return new Vector2(xPos, yPos);
    }

    private Vector2 GetActiveSpikeTrapDetectionOrigin()
    {
        float xPos = boxCollider.transform.position.x + boxCollider.bounds.extents.x + boxCollider.offset.x + 0.05f;
        float yPos = boxCollider.transform.position.y - boxCollider.bounds.extents.y + boxCollider.offset.y;
        return new Vector2(xPos, yPos);
    }

    private void OnDrawGizmosSelected()
    {
        DrawLedgeDetectionGizmo();
        DrawInactiveSpikeTrapDetectionGizmo();
        DrawActiveSpikeTrapDetectionGizmo();
    }

    private void DrawLedgeDetectionGizmo()
    {
        Gizmos.color = Color.red;
        Vector3 origin = GetLedgeDetectionOrigin();
        Gizmos.DrawLine(origin, origin - new Vector3(0f, 0.10f, 0f));
    }

    private void DrawInactiveSpikeTrapDetectionGizmo()
    {
        Gizmos.color = Color.red;
        Vector3 origin = GetInactiveSpikeTrapDetectionOrigin();
        Gizmos.DrawLine(origin, origin - new Vector3(0f, 0.05f, 0f));
    }

    private void DrawActiveSpikeTrapDetectionGizmo()
    {
        Gizmos.color = Color.red;
        Vector3 origin = GetSpikeTrapDetectionOrigin();
        Gizmos.DrawLine(origin, origin + new Vector3(0.05f, 0.0f, 0f));

    }

    private void HandleInactiveSpikeTrap()
    {
        Move(-1);
        aiIsDodgingWaitTimer = aiIsDodgingWaitTime;
    }

    private void HandleActiveSpikeTrap()
    {
        Move(0);
        aiIsDodgingWaitTimer = aiIsDodgingWaitTime / 2;
    }

    private bool IsDodging()
    {
        if (!isPlayer && aiIsDodgingWaitTimer > 0f)
        {
            aiIsDodgingWaitTimer -= Time.deltaTime;
            return true;
        }
        return false;
    }
}
