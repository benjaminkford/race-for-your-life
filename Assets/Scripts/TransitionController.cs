using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TransitionController : MonoBehaviour
{
    [SerializeField] private Image blackImage;
    [SerializeField] private bool startWithBlackScreen = true;

    private void Awake()
    {
        Time.timeScale = 1f;

        if (startWithBlackScreen)
        {
            blackImage.color = new Color(0f, 0f, 0f, 1f);
        }
    }

    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public IEnumerator FadeIn(float duration = 1f)
    {
        blackImage.color = new Color(0f, 0f, 0f, 1f);
        float time = 0f;

        while (time <= duration)
        {
            float alpha = Mathf.Lerp(1f, 0f, time / duration);
            blackImage.color = new Color(0f, 0f, 0f, alpha);
            time += Time.unscaledDeltaTime;
            yield return null;
        }

        blackImage.color = new Color(0f, 0f, 0f, 0f);
    }

    public IEnumerator FadeOut(float duration = 1f)
    {
        blackImage.color = new Color(0f, 0f, 0f, 0f);
        float time = 0f;

        while (time <= duration)
        {
            float alpha = Mathf.Lerp(0f, 1f, time / duration);
            blackImage.color = new Color(0f, 0f, 0f, alpha);
            time += Time.unscaledDeltaTime;
            yield return null;
        }

        blackImage.color = new Color(0f, 0f, 0f, 1f);
    }
}
