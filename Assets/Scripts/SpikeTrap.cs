using UnityEngine;
using UnityEngine.UI;

public enum TrapState
{
    Inactive,
    Active,
    Exhausted
};

public class SpikeTrap : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private GameObject rechargeBar;
    [SerializeField] private Image rechargeBarImage;

    private bool canTrigger = true;
    private float rechargeTime = 2.5f;
    private float rechargeTimer = 0f;

    public TrapState TrapState { get; private set; }

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (rechargeTimer > 0f)
        {
            rechargeTimer -= Time.deltaTime;
            rechargeBarImage.fillAmount = rechargeTimer / rechargeTime;
            if (rechargeTimer <= 0f)
            {
                TrapState = TrapState.Inactive;
                canTrigger = true;
                rechargeBar.SetActive(false);
            }
        }
    }

    public void Activate()
    {
        if (!canTrigger)
        {
            return;
        }

        animator.SetTrigger("activate");
        canTrigger = false;
        rechargeTimer = rechargeTime;
        rechargeBar.SetActive(true);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Racer racer = collision.gameObject.GetComponent<Racer>();
        if (racer && !racer.IsDead())
        {
            Activate();
        }
    }

    public bool CanTrigger()
    {
        return canTrigger;
    }
}
