using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform followTarget;

    private void LateUpdate()
    {
        if (followTarget != null)
        {
            transform.position = new Vector3(followTarget.position.x, transform.position.y, transform.position.z);
        }
    }

    public void SetTarget(Transform target)
    {
        followTarget = target;
    }

    public void MoveToPosition(Vector3 position)
    {
        followTarget = null;
        StartCoroutine(MoveToPositionCoroutine(position));
    }

    private IEnumerator MoveToPositionCoroutine(Vector3 position)
    {
        while (transform.position.x != position.x)
        {
            if (transform.position.x < position.x)
            {
                float distance = Mathf.Min(0.01f, position.x - transform.position.x);
                transform.position += new Vector3(distance, 0f, 0f);
            }
            else if (transform.position.x > position.x)
            {
                float distance = Mathf.Min(0.01f, transform.position.x - position.x);
                transform.position -= new Vector3(distance, 0f, 0f);
            }

            yield return null;
        }
    }
}
