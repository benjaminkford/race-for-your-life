using UnityEngine;

public class Ghost : MonoBehaviour
{
    private float MOVE_SPEED = 0.875f;

    private Fly fly;
    private SpriteController spriteController;
    private Interact interact;

    private bool isRaceFinished = false;

    private void Awake()
    {
        fly = GetComponent<Fly>();
        spriteController = GetComponent<SpriteController>();
        interact = GetComponent<Interact>();
    }

    private void OnEnable()
    {
        FinishLine.OnFinishLineCrossed += OnFinishLineCrossed;
        RaceController.OnAllRacersDied += OnAllRacersDied;
    }

    private void OnDisable()
    {
        FinishLine.OnFinishLineCrossed -= OnFinishLineCrossed;
        RaceController.OnAllRacersDied -= OnAllRacersDied;
    }

    private void Update()
    {
        if (isRaceFinished)
        {
            return;
        }

        PlayerUpdate();
    }

    private void PlayerUpdate()
    {
        Move();
        CheckInteract();
    }

    private void Move()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");
        int flip = 0;

        if (moveHorizontal > 0f)
        {
            flip = 1;
        }
        else if (moveHorizontal < 0f)
        {
            flip = -1;
        }

        fly.SetVelocity(MOVE_SPEED * moveHorizontal, MOVE_SPEED * moveVertical);
        spriteController.Flip(flip);
    }

    private void CheckInteract()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            interact.ShowRadius();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            interact.Activate();
        }
    }

    public void SetColor(Material material)
    {
        spriteController.SetMaterial(material);
    }

    private void OnFinishLineCrossed(Racer racer)
    {
        isRaceFinished = true;
        fly.SetVelocity(0f, 0f);
    }

    private void OnAllRacersDied()
    {
        isRaceFinished = true;
        fly.SetVelocity(0f, 0f);
    }
}
