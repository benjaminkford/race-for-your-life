using System.Collections;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private TransitionController transitionController;

    [SerializeField] private GameObject buttonsSection;
    [SerializeField] private GameObject prepSection;
    [SerializeField] private GameObject rulesSection;
    [SerializeField] private GameObject optionsSection;
    [SerializeField] private GameObject playerObject;
    [SerializeField] private SpriteRenderer playerSprite;

    private Material[] racerColors;
    private int playerColorIndex;

    private bool loadingGame = false;

    private void Start()
    {
        racerColors = GameSettings.Instance.GetRacerColors();
        playerColorIndex = GameSettings.Instance.GetPlayerColorIndex();

        playerSprite.material = racerColors[playerColorIndex];
    }

    public void PlayButton()
    {
        ToggleButtonsSection();
        TogglePrepSection();
    }

    public void RulesButton()
    {
        ToggleButtonsSection();
        ToggleRulesSection();
    }

    public void OptionsButton()
    {
        ToggleButtonsSection();
        ToggleOptionsSection();
    }

    public void OptionsBackButton()
    {
        ToggleButtonsSection();
        ToggleOptionsSection();
    }

    public void DecreaseSoundButton()
    {
        GameSettings.Instance.DecreaseVolume();
    }

    public void IncreaseSoundButton()
    {
        GameSettings.Instance.IncreaseVolume();
    }

    public void RulesBackButton()
    {
        ToggleButtonsSection();
        ToggleRulesSection();
    }

    public void LeftArrowButton()
    {
        if (loadingGame)
        {
            return;
        }

        playerColorIndex--;

        if (playerColorIndex < 0)
        {
            playerColorIndex = racerColors.Length - 1;
        }

        playerSprite.material = racerColors[playerColorIndex];
    }

    public void RightArrowButton()
    {
        if (loadingGame)
        {
            return;
        }

        playerColorIndex = (playerColorIndex + 1) % racerColors.Length;

        playerSprite.material = racerColors[playerColorIndex];
    }

    public void PlayBackButton()
    {
        if (loadingGame)
        {
            return;
        }

        ToggleButtonsSection();
        TogglePrepSection();
    }

    public void StartButton()
    {
        if (loadingGame)
        {
            return;
        }

        loadingGame = true;
        GameSettings.Instance.SetPlayerColor(playerColorIndex);
        StartCoroutine(StartGameCoroutine());
    }

    public void ToggleButtonsSection()
    {
        buttonsSection.SetActive(!buttonsSection.activeSelf);
    }

    public void TogglePrepSection()
    {
        prepSection.SetActive(!prepSection.activeSelf);
        playerObject.SetActive(!playerObject.activeSelf);
    }

    public void ToggleRulesSection()
    {
        rulesSection.SetActive(!rulesSection.activeSelf);
    }

    public void ToggleOptionsSection()
    {
        optionsSection.SetActive(!optionsSection.activeSelf);
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    private IEnumerator StartGameCoroutine()
    {
        yield return transitionController.FadeOut();

        transitionController.LoadScene("Race Scene");
    }
}
