using System.Collections;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private enum MenuState
    {
        Unpaused,
        Paused,
        Shutdown
    };

    [SerializeField] private TransitionController transitionController;
    [SerializeField] private GameObject menuObjectsSection;

    private MenuState state = MenuState.Unpaused;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    private void OnEnable()
    {
        FinishLine.OnFinishLineCrossed += OnFinishLineCrossed;
    }

    private void OnDisable()
    {
        FinishLine.OnFinishLineCrossed -= OnFinishLineCrossed;
    }

    private void OnFinishLineCrossed(Racer racer)
    {
        state = MenuState.Shutdown;
    }

    private void TogglePauseMenu()
    {
        if (state == MenuState.Shutdown)
        {
            return;
        }
        else if (state == MenuState.Unpaused)
        {
            state = MenuState.Paused;
            menuObjectsSection.SetActive(true);
            Time.timeScale = 0f;
        }
        else if (state == MenuState.Paused)
        {
            state = MenuState.Unpaused;
            menuObjectsSection.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void RestartButton()
    {
        if (state == MenuState.Shutdown)
        {
            return;
        }

        state = MenuState.Shutdown;
        menuObjectsSection.SetActive(false);

        StartCoroutine(RestartCoroutine());
    }

    private IEnumerator RestartCoroutine()
    {
        yield return transitionController.FadeOut();

        transitionController.LoadScene("Race Scene");
    }

    public void MainMenuButton()
    {
        if (state == MenuState.Shutdown)
        {
            return;
        }

        state = MenuState.Shutdown;
        menuObjectsSection.SetActive(false);

        StartCoroutine(MainMenuCoroutine());
    }

    private IEnumerator MainMenuCoroutine()
    {
        yield return transitionController.FadeOut();

        transitionController.LoadScene("Main Menu");
    }
}
