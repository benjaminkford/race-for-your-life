using UnityEngine;

public class Interact : MonoBehaviour
{
    [SerializeField] private GameObject interactCircle;

    public void ShowRadius()
    {
        interactCircle.SetActive(true);
    }

    public void Activate()
    {
        CircleCollider2D collider = interactCircle.GetComponent<CircleCollider2D>();
        LayerMask trapLayer = LayerMask.GetMask("ActionableTrap");
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(trapLayer);

        Collider2D[] results = Physics2D.OverlapCircleAll(transform.position, collider.radius, trapLayer.value);
        interactCircle.SetActive(false);
        Debug.Log(results.Length);

        if (results.Length == 0)
        {
            return;
        }

        SpikeTrap trap;
        float closestDistance = Vector2.Distance(transform.position, results[0].transform.position);
        int closestIndex = 0;

        for (int idx = 1; idx < results.Length; idx++)
        {
            trap = results[idx].GetComponent<SpikeTrap>();
            if (!trap.CanTrigger())
            {
                continue;
            }

            float distance = Vector2.Distance(transform.position, results[idx].transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIndex = idx;
            }
        }

        trap = results[closestIndex].gameObject.GetComponent<SpikeTrap>();
        trap.Activate();

        interactCircle.SetActive(false);
    }
}
