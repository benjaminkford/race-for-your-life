using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private BoxCollider2D groundCheck;
    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public bool IsGrounded()
    {
        /* Not happy about this... but it solves the jump SFX being
         * played when the computer AI lands at times, so...
         */
        if (rb2d.velocity.y < -0.1f)
        {
            return false;
        }

        Collider2D[] results = new Collider2D[1];
        LayerMask mask = LayerMask.GetMask("Ground");
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(mask);

        if (groundCheck.OverlapCollider(filter, results) > 0)
        {
            return true;
        }

        return false;
    }

    public void AddForce(float val)
    {
        rb2d.AddForce(new Vector2(0f, val));
    }
}
