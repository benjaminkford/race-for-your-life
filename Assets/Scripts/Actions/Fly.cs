using UnityEngine;

public class Fly : MonoBehaviour
{
    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public void SetVelocity(float xVelocity, float yVelocity)
    {
        rb2d.velocity = new Vector2(xVelocity, yVelocity);
    }
}
