using UnityEngine;

public class Run : MonoBehaviour
{
    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public void SetVelocity(float val)
    {
        rb2d.velocity = new Vector2(val, rb2d.velocity.y);
    }

    public void Freeze()
    {
        rb2d.gravityScale = 0f;
        rb2d.velocity = Vector2.zero;
    }
}
