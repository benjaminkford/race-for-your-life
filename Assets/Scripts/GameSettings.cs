using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public static GameSettings Instance;

    public delegate void VolumeChanged(float volume);
    public static VolumeChanged OnVolumeChanged;

    [SerializeField] private Material[] racerColors;
    private int playerColorIndex;

    private float volume = 0.5f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            playerColorIndex = 0;
            return;
        }

        Destroy(gameObject);
    }

    public int GetPlayerColorIndex()
    {
        return playerColorIndex;
    }

    public Material GetPlayerColor()
    {
        return racerColors[playerColorIndex];
    }

    public Material[] GetRacerColors()
    {
        return racerColors;
    }

    public void SetPlayerColor(int index)
    {
        playerColorIndex = index;
    }

    public float GetVolume()
    {
        return volume;
    }

    public void DecreaseVolume()
    {
        volume = Mathf.Clamp(volume - 0.1f, 0f, 0.5f);
        OnVolumeChanged?.Invoke(volume);
    }

    public void IncreaseVolume()
    {
        volume = Mathf.Clamp(volume + 0.1f, 0f, 0.5f);
        OnVolumeChanged?.Invoke(volume);
    }
}
