using UnityEngine;

public class BGMPlayer : MonoBehaviour
{
    public static BGMPlayer Instance;

    private AudioSource audioSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            return;
        }

        Destroy(gameObject);
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = GameSettings.Instance.GetVolume();
    }

    private void OnEnable()
    {
        GameSettings.OnVolumeChanged += OnVolumeChanged;
    }

    private void OnDisable()
    {
        GameSettings.OnVolumeChanged -= OnVolumeChanged;
    }

    private void OnVolumeChanged(float volume)
    {
        audioSource.volume = volume;
    }
}
