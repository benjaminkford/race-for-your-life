using UnityEngine;

public class Hazard : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Racer racer = collision.gameObject.GetComponent<Racer>();
        if (racer)
        {
            racer.Kill();
        }
    }
}
