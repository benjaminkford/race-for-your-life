using UnityEngine;
using TMPro;
using System.Collections;

public class RaceFinishUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI winLoseText;
    [SerializeField] private GameObject buttonsSection;
    [SerializeField] private TransitionController transitionController;

    private bool isLoading = false;

    public void SetText(string text)
    {
        winLoseText.gameObject.SetActive(true);
        winLoseText.text = text;
    }

    public void ShowButtons()
    {
        buttonsSection.SetActive(true);
    }

    public void PlayAgainButton()
    {
        if (isLoading)
        {
            return;
        }

        StartCoroutine(LoadNextScene("Race Scene"));
    }

    public void MainMenuButton()
    {
        if (isLoading)
        {
            return;
        }

        StartCoroutine(LoadNextScene("Main Menu"));
    }

    private IEnumerator LoadNextScene(string sceneName)
    {
        yield return transitionController.FadeOut();

        transitionController.LoadScene(sceneName);
    }
}
