using UnityEngine;

public class SpriteController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Flip(int direction)
    {
        if (direction == -1)
        {
            spriteRenderer.flipX = true;
        }
        else if (direction == 1)
        {
            spriteRenderer.flipX = false;
        }
    }

    public void SetBool(string name, bool val)
    {
        animator.SetBool(name, val);
    }

    public void SetTrigger(string name)
    {
        animator.SetTrigger(name);
    }

    public Material GetMaterial()
    {
        return spriteRenderer.material;
    }

    public void SetMaterial(Material material)
    {
        spriteRenderer.material = new Material(material);
    }

    public void SetSortOrder(int num)
    {
        spriteRenderer.sortingOrder = num;
    }
}
