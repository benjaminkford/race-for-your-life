using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public delegate void FinishLineCrossed(Racer racer);
    public static event FinishLineCrossed OnFinishLineCrossed;

    [SerializeField] private SpriteRenderer spriteRenderer;

    private bool finishLinePassed = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (finishLinePassed)
        {
            return;
        }

        Racer racer = collision.gameObject.GetComponent<Racer>();
        if (racer)
        {
            spriteRenderer.material = new Material(racer.GetColor());
            finishLinePassed = true;

            OnFinishLineCrossed?.Invoke(racer);
        }
    }
}
