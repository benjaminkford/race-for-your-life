using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaceController : MonoBehaviour
{
    public delegate void AllRacersDied();
    public static AllRacersDied OnAllRacersDied;

    [SerializeField] private GameObject racerPrefab;
    [SerializeField] private TransitionController transitionController;
    [SerializeField] private CameraController cameraController;
    [SerializeField] private RaceFinishUI raceFinishUI;
    [SerializeField] private TextMeshProUGUI countdownText;

    private List<Racer> racers;
    private int numberOfDeadRacers = 0;

    private void Start()
    {
        CreateRacers(3);

        cameraController.SetTarget(racers[0].transform);

        StartCoroutine(StartRace());
    }

    private void OnEnable()
    {
        FinishLine.OnFinishLineCrossed += OnFinishLineCrossed;
        Racer.OnRacerDied += OnRacerDied;
    }

    private void OnDisable()
    {
        FinishLine.OnFinishLineCrossed -= OnFinishLineCrossed;
        Racer.OnRacerDied -= OnRacerDied;
    }

    private IEnumerator StartRace()
    {
        yield return transitionController.FadeIn();
        yield return new WaitForSeconds(1f);

        countdownText.gameObject.SetActive(true);
        countdownText.text = "3";
        yield return new WaitForSeconds(1f);

        countdownText.text = "2";
        yield return new WaitForSeconds(1f);

        countdownText.text = "1";
        yield return new WaitForSeconds(1f);

        countdownText.text = "Go!";
        foreach (Racer racer in racers)
        {
            racer.SetStateRacing();
        }
        yield return new WaitForSeconds(1f);

        countdownText.gameObject.SetActive(false);
    }

    private void CreateRacers(int computerRacers)
    {
        racers = new List<Racer>();
        int playerColorIndex = GameSettings.Instance.GetPlayerColorIndex();
        Material[] racerColors = GameSettings.Instance.GetRacerColors();

        Racer racer = SpawnRacer(new Vector3(-1.2f, -0.53f, 0f), true);
        racers.Add(racer);

        for (int idx = 1; idx <= computerRacers; idx++)
        {
            racer = SpawnRacer(new Vector3(-1.36f, 0.43f, 0f) + new Vector3(0.16f * idx, 0f, 0f));
            racers.Add(racer);
            int colorIdx = (playerColorIndex + idx) % racerColors.Length;
            racer.SetColor(racerColors[colorIdx]);
        }
    }

    private Racer SpawnRacer(Vector3 position, bool isPlayer = false)
    {
        Racer racer = Instantiate(racerPrefab, position, Quaternion.identity).GetComponent<Racer>();
        
        if (isPlayer)
        {
            racer.SetPlayer();
            racer.SetColor(GameSettings.Instance.GetPlayerColor());
        }

        return racer;
    }

    private void OnFinishLineCrossed(Racer racer)
    {
        raceFinishUI.SetText(racer.IsPlayer() ? "You Win!" : "Try again?");
        if (!racer.IsPlayer())
        {
            cameraController.MoveToPosition(racer.transform.position);
        }

        StartCoroutine(EndOfRaceRoutine());
    }

    private IEnumerator EndOfRaceRoutine()
    {
        yield return new WaitForSeconds(1f);

        raceFinishUI.ShowButtons();
    }

    private void OnRacerDied()
    {
        numberOfDeadRacers++;

        if (numberOfDeadRacers == racers.Count)
        {
            OnAllRacersDied?.Invoke();

            raceFinishUI.SetText("You win!");

            StartCoroutine(EndOfRaceRoutine());
        }
    }
}
